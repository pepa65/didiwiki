// didi.c
// DidiWiki - a small lightweight wiki engine
// Copyright 2004 Matthew Allum <mallum@o-hand.com> GPL-2+

#include "didi.h"

char ip[16] = "0.0.0.0";

void usage()
{
	fprintf(stderr, "Usage:  %s [IP | help | debug]\n", PACKAGE);
	exit(1);
}

int main(int argc, char **argv)
{
	HttpRequest *req=NULL;
	wiki_init();
	// reads request from stdin, or forks
	if (argc > 1 && !strcmp(argv[1],"debug")) req=http_request_new();
	else if (argc > 1 && !strcmp(argv[1],"help")) usage();
	else if (argc > 1) strncpy(ip, argv[1], 16);
	req=http_server();
	wiki_handle_http_request(req);
	return 0;
}
